<?php
namespace Extranet\MailBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Mail
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\User")
     */
    private $from;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Extranet\DashboardBundle\Document\User")
     */
    private $to;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\MailBundle\Document\Mail")
     */
    private $replyTo;

    /** @MongoDB\Field(type="string") */
    private $title;

    /** @MongoDB\Field(type="boolean") */
    private $status;


    /** @MongoDB\Field(type="string") */
    private $body;
    
    /**
     * @MongoDB\Date
     */
    private $created;

     /**
     * @MongoDB\ReferenceMany(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
     private $attachment;

    public function getId(){return $this->id;}

    public function getTitle(){return $this->title;}

    public function setTitle($value){$this->title=$value;}

    public function getStatus(){return $this->status;}

    public function setStatus($value){$this->status=$value;}

    public function getFrom(){return $this->from;}

    public function setFrom($value){$this->from=$value;}

    public function getTo(){return $this->to;}

    public function setTo($value){$this->to=$value;}

    public function getBody(){return $this->body;}

    public function setBody($value){$this->body=$value;}

    public function getAttachment(){return $this->attachment;}

    public function setAttachment($value){$this->attachment=$value;}

    public function getReplyTo(){return $this->replyTo;}

    public function setReplyTo($value){$this->replyTo=$value;}

    public function getCreated(){return $this->created;}

    public function setCreated(){$this->created=date('Y-m-d');}
}
