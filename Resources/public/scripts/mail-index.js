function onCreateMailButtonClick(event) {
    if ($(this).hasClass("reply-to-button"))
    {
        form = $(this).parents('li').children('.bg-forms');
        if (form.is(':hidden')) {
            form.slideDown('fast');
        }
        else {
            form.slideUp('fast');
        }
    }
    else
    {
        $('.bg-forms:not(:first)').hide();
        $('.active').toggleClass('active');
        form = $('.bg-forms:first');
        if (form.is(':hidden')){
            form.slideDown('fast');
            $(this).children('i').removeClass('icon-plus');
            $(this).children('i').addClass('icon-minus');
        }else{
            form.slideUp('fast');
            $(this).children('i').removeClass('icon-minus');
            $(this).children('i').addClass('icon-plus');
        }
    }
};

$(document).ready(function(){
    // Affiche ou Cache le formulaire d'envoi de mail
    $('#display-add, .reply-to-button').click(onCreateMailButtonClick);

    $('.accordion li:first .bg-forms').hide();
});
