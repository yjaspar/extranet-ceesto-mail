<?php

namespace Extranet\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

use Extranet\MailBundle\Document\Mail as Mail;
use Extranet\DashboardBundle\Document\User as User;

class DefaultController extends Controller
{
  // Private methods

  private function retrieveMailsForUser(User $user)
  {
    $dm = $this->get('doctrine_mongodb')->getManager();
    $qb = $dm->createQueryBuilder('ExtranetMailBundle:Mail');
    $qb->expr()->field('to')->elemMatch($qb->expr()->field('$id')->equals(new \MongoId($user->getId())));
    return $qb->getQuery()->execute();
  }

  private function retrieveAndRenderMailsForUser(User $user) {
    $mails = $this->retrieveMailsForUser($user);
    $count = ($mails != null && $mails->count() != 0) ? $mails->count() : 0;
    return $this->render('ExtranetMailBundle:Default:index.html.twig', array('self' => $user, 'title' => 'Dashboard', 'mails' => $mails, 'count' => $count));
  }

  private function createNewMail($title, $status, $from, $to, $body, $replyto, $attachment){
    $mail= new Mail();
    $mail->setTitle($title);
    $mail->setStatus(false);
    $mail->setFrom($from);
    $mail->setTo($to);
    $mail->setBody($body);
    $mail->setCreated();
    $mail->setReplyTo($replyto);
    $mail->setAttachment($attachment);
    return ($mail);
  }

  // Rest public methods

  public function indexAction()
  {
    $request = Request::createFromGlobals();
    $current_user = $this->get('security.context')->getToken()->getUser();

    $admin = ($current_user->getRoles()[0] == 'ROLE_ADMIN' || $current_user->getRoles()[0] == 'ROLE_SUPER_ADMIN');
    if ($request->isXmlHttpRequest()){
        $response = new Response();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $qb = $dm->createQueryBuilder('ExtranetMailBundle:Mail');
        $qb->expr()->field('to')->equals($current_user);
        $qb->field('status')->equals(false);

        $mails = $qb->getQuery()->execute();

        $response->headers->set('Content-Type', 'application/json');
        $response->setContent('{"responseCode":200, "responseText":{"mailNumber":'.$mails->count().'}}');
        return $response;
    } else {
        $mails = $this->retrieveMailsForUser($current_user);
        $count = ($mails != null && $mails->count() != 0) ? $mails->count() : 0;
        return $this->render('ExtranetMailBundle:Default:index.html.twig', array('self' => $current_user, 'admin' => $admin, 'title' => 'Dashboard', 'mails' => $mails, 'count' => $count));
    }
  }

    public function setMailReadAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();

        $mail = $dm->getRepository('ExtranetMailBundle:Mail')->findOneById($_POST['id']);

        $mail->setStatus(true);

        $dm->persist($mail);
        $dm->flush();
        
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent('{"responseCode":200, "responseText":"Success"');
        return $response;
    }

  public function editAction($id)
  {
    $data = $this->getRequest()->request;

    $adresses = $data->get("adresses");
    $title = $data->get("title");
    $body = $data->get("body");

    $dm = $this->get('doctrine_mongodb')->getManager();

    $attachment = array();

    foreach($_FILES as $file){
        if ($file['error'] === UPLOAD_ERR_OK){
            $tmp = new \Extranet\DashboardBundle\Document\Upload();
            $tmp->setFile($file['tmp_name']);
            $tmp->setFileName($file['name']);
            $tmp->setMimeType($file['type']);
            $tmp->setLength($file['size']);

            $dm->persist($tmp);
            $dm->flush();

            array_push($attachment, $tmp);
        }
    }

    $replyto = $dm->getRepository('ExtranetMailBundle:Mail')->find($id);
    $adresses = explode(';', $adresses);
    $current_user = $this->get('security.context')->getToken()->getUser();
    $to = array();
    foreach ($adresses as $adresse) {
      $userFound = $dm->getRepository('ExtranetDashboardBundle:User')->findByEmail($adresse);
      foreach ($userFound as $user) {
        $to[] = $user;
      }
    }

    $mail = $this->createNewMail($title, false, $current_user, $to, $body, $replyto, $attachment);

    $dm->persist($mail);
    $dm->flush();

    return $this->retrieveAndRenderMailsForUser($current_user);
  }


  public function deleteAction($id)
  {
    $request = Request::createFromGlobals();

    $dm = $this->get('doctrine_mongodb')->getManager();
    $qb = $dm->createQueryBuilder('ExtranetMailBundle:Mail');
    $qb->field('id')->equals($id)->remove();
    $qb->getQuery()->execute();
    $dm->flush();
    $current_user = $this->get('security.context')->getToken()->getUser();
    $mails = $this->retrieveMailsForUser($current_user);
    $count = ($mails != null && $mails->count() != 0) ? $mails->count() : 0;
    if ($request->isXmlHttpRequest()){
      $response = new Response();
      $response->headers->set('Content-Type', 'text/html');
      $response->setContent('{"responseCode":200, "responseText":{"mail deleted"}}');
      $response->setContent('{"responseCode":200, "responseText":{"mailNumber":'.$count.'}}');
      return $response;
    } else {
      return $this->render('ExtranetMailBundle:Default:index.html.twig', array('self' => $current_user, 'title' => 'Dashboard', 'mails' => $mails, 'count' => $count));
    }
  }

  public function newAction() {
    $data = $this->getRequest()->request;
    var_dump($_POST);
    var_dump($_FILES);
    exit(0);
    $adresses = $data->get("adresses");
    $title = $data->get("title");
    $body = $data->get("body");

    $dm = $this->get('doctrine_mongodb')->getManager();

    $attachment = array();

    foreach($_FILES as $file){
        $tmp = new \Extranet\DashboardBundle\Document\Upload();
        $tmp->setFile($file['tmp_name']);
        $tmp->setFileName($file['name']);
        $tmp->setMimeType($file['type']);
        $tmp->setLength($file['size']);

        $dm->persist($tmp);
        $dm->flush();

        array_push($attachment, $tmp);
    }

    $adresses = explode(';', $adresses);
    $current_user = $this->get('security.context')->getToken()->getUser();
    $to = array();
    foreach ($adresses as $adresse) {
      $userFound = $dm->getRepository('ExtranetDashboardBundle:User')->findByEmail($adresse);
      foreach ($userFound as $user) {
        $to[] = $user;
      }
    }
    $mail = $this->createNewMail($title, false, $current_user, $to, $body, null, $attachment);

    $dm->persist($mail);
    $dm->flush();

    return $this->retrieveAndRenderMailsForUser($current_user);
  }
}
